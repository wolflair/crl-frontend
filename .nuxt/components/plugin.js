import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  Logo: () => import('../..\\components\\Logo.vue' /* webpackChunkName: "components/logo" */).then(c => wrapFunctional(c.default || c)),
  Pagination: () => import('../..\\components\\Pagination.vue' /* webpackChunkName: "components/pagination" */).then(c => wrapFunctional(c.default || c)),
  FormsCheckbox: () => import('../..\\components\\forms\\Checkbox.vue' /* webpackChunkName: "components/forms-checkbox" */).then(c => wrapFunctional(c.default || c)),
  FormsDropdown: () => import('../..\\components\\forms\\Dropdown.vue' /* webpackChunkName: "components/forms-dropdown" */).then(c => wrapFunctional(c.default || c)),
  FormsDropdownDepartment: () => import('../..\\components\\forms\\DropdownDepartment.vue' /* webpackChunkName: "components/forms-dropdown-department" */).then(c => wrapFunctional(c.default || c)),
  FormsDropdownOfficers: () => import('../..\\components\\forms\\DropdownOfficers.vue' /* webpackChunkName: "components/forms-dropdown-officers" */).then(c => wrapFunctional(c.default || c)),
  FormsRadio: () => import('../..\\components\\forms\\Radio.vue' /* webpackChunkName: "components/forms-radio" */).then(c => wrapFunctional(c.default || c)),
  MenusNavbar: () => import('../..\\components\\menus\\Navbar.vue' /* webpackChunkName: "components/menus-navbar" */).then(c => wrapFunctional(c.default || c)),
  MenusResignedTable: () => import('../..\\components\\menus\\Resigned-Table.vue' /* webpackChunkName: "components/menus-resigned-table" */).then(c => wrapFunctional(c.default || c)),
  MenusSidebar: () => import('../..\\components\\menus\\Sidebar.vue' /* webpackChunkName: "components/menus-sidebar" */).then(c => wrapFunctional(c.default || c)),
  MenusTable: () => import('../..\\components\\menus\\Table.vue' /* webpackChunkName: "components/menus-table" */).then(c => wrapFunctional(c.default || c)),
  ModalsAddEmployee: () => import('../..\\components\\modals\\AddEmployee.vue' /* webpackChunkName: "components/modals-add-employee" */).then(c => wrapFunctional(c.default || c)),
  ModalsAddLeaveList: () => import('../..\\components\\modals\\AddLeaveList.vue' /* webpackChunkName: "components/modals-add-leave-list" */).then(c => wrapFunctional(c.default || c)),
  ModalsFilter: () => import('../..\\components\\modals\\Filter.vue' /* webpackChunkName: "components/modals-filter" */).then(c => wrapFunctional(c.default || c)),
  ModalsUpdate: () => import('../..\\components\\modals\\Update.vue' /* webpackChunkName: "components/modals-update" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
