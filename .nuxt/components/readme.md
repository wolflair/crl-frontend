# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Logo>` | `<logo>` (components/Logo.vue)
- `<Pagination>` | `<pagination>` (components/Pagination.vue)
- `<FormsCheckbox>` | `<forms-checkbox>` (components/forms/Checkbox.vue)
- `<FormsDropdown>` | `<forms-dropdown>` (components/forms/Dropdown.vue)
- `<FormsDropdownDepartment>` | `<forms-dropdown-department>` (components/forms/DropdownDepartment.vue)
- `<FormsDropdownOfficers>` | `<forms-dropdown-officers>` (components/forms/DropdownOfficers.vue)
- `<FormsRadio>` | `<forms-radio>` (components/forms/Radio.vue)
- `<MenusNavbar>` | `<menus-navbar>` (components/menus/Navbar.vue)
- `<MenusResignedTable>` | `<menus-resigned-table>` (components/menus/Resigned-Table.vue)
- `<MenusSidebar>` | `<menus-sidebar>` (components/menus/Sidebar.vue)
- `<MenusTable>` | `<menus-table>` (components/menus/Table.vue)
- `<ModalsAddEmployee>` | `<modals-add-employee>` (components/modals/AddEmployee.vue)
- `<ModalsAddLeaveList>` | `<modals-add-leave-list>` (components/modals/AddLeaveList.vue)
- `<ModalsFilter>` | `<modals-filter>` (components/modals/Filter.vue)
- `<ModalsUpdate>` | `<modals-update>` (components/modals/Update.vue)
