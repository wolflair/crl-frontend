import { wrapFunctional } from './utils'

export { default as Logo } from '../..\\components\\Logo.vue'
export { default as Pagination } from '../..\\components\\Pagination.vue'
export { default as FormsCheckbox } from '../..\\components\\forms\\Checkbox.vue'
export { default as FormsDropdown } from '../..\\components\\forms\\Dropdown.vue'
export { default as FormsDropdownDepartment } from '../..\\components\\forms\\DropdownDepartment.vue'
export { default as FormsDropdownOfficers } from '../..\\components\\forms\\DropdownOfficers.vue'
export { default as FormsRadio } from '../..\\components\\forms\\Radio.vue'
export { default as MenusNavbar } from '../..\\components\\menus\\Navbar.vue'
export { default as MenusResignedTable } from '../..\\components\\menus\\Resigned-Table.vue'
export { default as MenusSidebar } from '../..\\components\\menus\\Sidebar.vue'
export { default as MenusTable } from '../..\\components\\menus\\Table.vue'
export { default as ModalsAddEmployee } from '../..\\components\\modals\\AddEmployee.vue'
export { default as ModalsAddLeaveList } from '../..\\components\\modals\\AddLeaveList.vue'
export { default as ModalsFilter } from '../..\\components\\modals\\Filter.vue'
export { default as ModalsUpdate } from '../..\\components\\modals\\Update.vue'

export const LazyLogo = import('../..\\components\\Logo.vue' /* webpackChunkName: "components/logo" */).then(c => wrapFunctional(c.default || c))
export const LazyPagination = import('../..\\components\\Pagination.vue' /* webpackChunkName: "components/pagination" */).then(c => wrapFunctional(c.default || c))
export const LazyFormsCheckbox = import('../..\\components\\forms\\Checkbox.vue' /* webpackChunkName: "components/forms-checkbox" */).then(c => wrapFunctional(c.default || c))
export const LazyFormsDropdown = import('../..\\components\\forms\\Dropdown.vue' /* webpackChunkName: "components/forms-dropdown" */).then(c => wrapFunctional(c.default || c))
export const LazyFormsDropdownDepartment = import('../..\\components\\forms\\DropdownDepartment.vue' /* webpackChunkName: "components/forms-dropdown-department" */).then(c => wrapFunctional(c.default || c))
export const LazyFormsDropdownOfficers = import('../..\\components\\forms\\DropdownOfficers.vue' /* webpackChunkName: "components/forms-dropdown-officers" */).then(c => wrapFunctional(c.default || c))
export const LazyFormsRadio = import('../..\\components\\forms\\Radio.vue' /* webpackChunkName: "components/forms-radio" */).then(c => wrapFunctional(c.default || c))
export const LazyMenusNavbar = import('../..\\components\\menus\\Navbar.vue' /* webpackChunkName: "components/menus-navbar" */).then(c => wrapFunctional(c.default || c))
export const LazyMenusResignedTable = import('../..\\components\\menus\\Resigned-Table.vue' /* webpackChunkName: "components/menus-resigned-table" */).then(c => wrapFunctional(c.default || c))
export const LazyMenusSidebar = import('../..\\components\\menus\\Sidebar.vue' /* webpackChunkName: "components/menus-sidebar" */).then(c => wrapFunctional(c.default || c))
export const LazyMenusTable = import('../..\\components\\menus\\Table.vue' /* webpackChunkName: "components/menus-table" */).then(c => wrapFunctional(c.default || c))
export const LazyModalsAddEmployee = import('../..\\components\\modals\\AddEmployee.vue' /* webpackChunkName: "components/modals-add-employee" */).then(c => wrapFunctional(c.default || c))
export const LazyModalsAddLeaveList = import('../..\\components\\modals\\AddLeaveList.vue' /* webpackChunkName: "components/modals-add-leave-list" */).then(c => wrapFunctional(c.default || c))
export const LazyModalsFilter = import('../..\\components\\modals\\Filter.vue' /* webpackChunkName: "components/modals-filter" */).then(c => wrapFunctional(c.default || c))
export const LazyModalsUpdate = import('../..\\components\\modals\\Update.vue' /* webpackChunkName: "components/modals-update" */).then(c => wrapFunctional(c.default || c))
