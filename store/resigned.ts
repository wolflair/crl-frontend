// @ts-nocheck
export const state = () => {
  return {
    officers: [],
  }
}

export const mutations = {
  fetchResignedOfficers(state, data) {
    state.officers = data
  },
}

export const actions = {
  // GET ActiveOfficers
  async fetchResignedOfficers(context) {
    const res = await this.$axios('https://crl-backend.herokuapp.com/resigned')
    context.commit('fetchResignedOfficers', res.data)
  },

  // DELETE
  async deleteEmployees(context, id) {
    await this.$axios.delete(
      `https://crl-backend.herokuapp.com/officer?id=${id}`
    )
    const res = await this.$axios('https://crl-backend.herokuapp.com/resigned')
    context.commit('fetchResignedOfficers', res.data)
  },

  // SEARCH OFFICER
  async findOfficerByIdCard(context, data) {
    await this.$axios(
      `https://crl-backend.herokuapp.com/officer-search?=${{
        idCard: data.idCard,
      }}`
    )
    const res = await this.$axios('https://crl-backend.herokuapp.com/resigned')
    context.commit('fetchResignedOfficers', res.data)
  },
}

export const getters = {
  getData: (state: any) => state.officers,
}
