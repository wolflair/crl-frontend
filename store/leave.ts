// @ts-nocheck
export const state = () => {
  return {
    officers: [],
  }
}

export const mutations = {
  fetchLeaveOfficers(state, data) {
    state.officers = data
  },
}

export const actions = {
  // GET ActiveOfficers
  async fetchLeaveOfficers(context) {
    const res = await this.$axios('https://crl-backend.herokuapp.com/active-leave')
    context.commit('fetchLeaveOfficers', res.data)
  },

  // DELETE
  async deleteLeave(context, id) {
    await this.$axios.delete(`https://crl-backend.herokuapp.com/leave?id=${id}`)
    const res = await this.$axios('https://crl-backend.herokuapp.com/leaves')
    context.commit('fetchActiveOfficers', res.data)
  },

  // PUT LEAVE
  async putAddLeave(context, data) {
    await this.$axios.put(`https://crl-backend.herokuapp.com/add-leaves`, {
      idCard: data.idCard,
      leave: data.leave,
      personalLeave: data.personalLeave,
      sickLeave: data.sickLeave,
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active-leave')
    context.commit('fetchLeaveOfficers', res.data)
  },

  // PUT LEAVE
  async putLeaveResinged(context, data) {
    await this.$axios.put(`https://crl-backend.herokuapp.com/leave`, {
      idCard: data.idCard,
      leave: data.leave,
      personalLeave: data.personalLeave,
      sickLeave: data.sickLeave,
      status: 'resinged',
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active-leave')
    context.commit('fetchLeaveOfficers', res.data)
  },
}
export const getters = {
  getData: (state: any) => state.officers,
}
