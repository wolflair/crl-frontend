// @ts-nocheck
export const state = () => {
  return {
    officers: [],
  }
}

export const mutations = {
  fetchLeaveListOfficers(state, data) {
    state.officers = data
  },
}

export const actions = {
  // GET ActiveOfficers
  async fetchLeaveListOfficers(context) {
    const res = await this.$axios(
      'https://crl-backend.herokuapp.com/leave-lists'
    )
    context.commit('fetchLeaveListOfficers', res.data)
  },

  // CREATE Leave List
  async addLeaveList(context, data) {
    await this.$axios.post('https://crl-backend.herokuapp.com/leave-list', {
      idCard: data.idCard,
      leave: data.leave,
      personalLeave: data.personalLeave,
      sickLeave: data.sickLeave,
      description: data.description,
    })
    const res = await this.$axios(
      'https://crl-backend.herokuapp.com/leave-lists'
    )
    context.commit('fetchLeaveListOfficers', res.data)
  },
}
export const getters = {
  getData: (state: any) => state.officers,
}
