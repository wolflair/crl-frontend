// @ts-nocheck
export const state = () => {
  return {
    officers: [],
  }
}

export const mutations = {
  fetchActiveOfficers(state, data) {
    state.officers = data
  },
}

export const actions = {
  // GET ActiveOfficers
  async fetchActiveOfficers(context) {
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },

  // CREATE
  async addEmployees(context, data) {
    await this.$axios.post('https://crl-backend.herokuapp.com/officer', {
      idCard: data.idCard,
      firstName: data.firstName,
      lastName: data.lastName,
      position: data.position,
      department: data.department,
      status: 'active',
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },

  // CREATE Leave
  async addLeave(context, data) {
    await this.$axios.post('https://crl-backend.herokuapp.com/leave', {
      idCard: data.idCard,
      leave: 0,
      personalLeave: 0,
      sickLeave: 0,
      remainingLeave: data.remainingLeave,
      status: 'active',
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },

  // PUT
  async putEmployees(context, data) {
    await this.$axios.put(`https://crl-backend.herokuapp.com/officer`, {
      _id: data._id,
      idCard: data.idCard,
      firstName: data.firstName,
      lastName: data.lastName,
      position: data.position,
      department: data.department,
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },

  // PUT LEAVE
  async putLeave(context, data) {
    await this.$axios.put('https://crl-backend.herokuapp.com/leave', {
      idCard: data.idCard,
      leave: data.leave,
      personalLeave: data.personalLeave,
      sickLeave: data.sickLeave,
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active-leave')
    context.commit('fetchActiveOfficers', res.data)
  },

  // LEAVE
  async leaveCompany(context, data) {
    await this.$axios.put(`https://crl-backend.herokuapp.com/officer`, {
      _id: data._id,
      status: 'resigned',
    })
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },

  // SEARCH OFFICER
  async findOfficerByIdCard(context, data) {
    await this.$axios(
      `https://crl-backend.herokuapp.com/officer-search?=${{
        idCard: data.idCard,
      }}`
    )
    const res = await this.$axios('https://crl-backend.herokuapp.com/active')
    context.commit('fetchActiveOfficers', res.data)
  },
}

export const getters = {
  getData: (state: any) => state.officers,
}
