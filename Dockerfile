FROM node:lts

ENV APP_DIR /usr/src/app/client
WORKDIR ${APP_DIR}

COPY . .

RUN yarn install

RUN yarn build

RUN yarn generate

ENV HOST 0.0.0.0
EXPOSE 3000

CMD [ "yarn", "start" ]